# Include standard NID (NSO in Docker) package Makefile that defines all
# standard make targets
include nidpackage.mk

# The rest of this file is specific to this repository.

testenv-start-extra:
	@echo "\n== Starting repository specific testenv"
	docker run -td --name $(CNT_PREFIX)-jaeger --network-alias jaeger $(DOCKER_ARGS) jaegertracing/all-in-one:latest
	docker run -td --name $(CNT_PREFIX)-grafana --network-alias grafana $(DOCKER_ARGS) grafana/grafana
	docker exec -t $(CNT_PREFIX)-nso bash -lc 'ncs --wait-started 600'
	$(MAKE) testenv-runcmdJ CMD="unhide debug\nconfigure\nedit progress opentracing\n set enabled\n set reporting-host jaeger\ncommit"
	$(MAKE) testenv-print-jaeger-address

testenv-print-jaeger-address:
	@echo "Visit the following URL in your web browser to reach Jaeger:"
	@docker inspect $(CNT_PREFIX)-jaeger | sed -ne '/Networks.:/,/^            }/p' | awk '/IPAddress/ { print $$2 }' | sed 's/[",]//g' | awk '{ print "http://"$$1":16686"}'


testenv-test:
	@echo "\n== Running tests"
	$(MAKE) testenv-test-concurrent

testenv-test-simple:
	$(MAKE) testenv-runcmdJ CMD="configure\ndelete slow-service\ncommit"
	$(MAKE) testenv-runcmdJ CMD="configure\nset slow-service FOO\ncommit"

TEST_INSTANCES=A B C

testenv-test-concurrent:
	$(MAKE) testenv-runcmdJ CMD="configure\ndelete slow-service\ncommit"
	$(MAKE) -j20 testenv-test-concurrent-run

testenv-test-concurrent-run: $(addsuffix testenv-test-concurrent,$(TEST_INSTANCES))

$(addsuffix testenv-test-concurrent,$(TEST_INSTANCES)):
	$(MAKE) testenv-runcmdJ CMD="configure\nset slow-service $(subst testenv-test-concurrent,,$@) create-slowness 2\ncommit"

