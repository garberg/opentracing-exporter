"""NSO progress-trace related stuffz
"""
import csv
import datetime
import logging
import re

from opentelemetry import trace
from opentelemetry.ext import jaeger
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import SimpleExportSpanProcessor

def process_traces(tracer, stream):
    """Process a stream of progress-trace events and export to tracing system

    This expects a generator as input that will keep yielding progress-trace
    events.
    """
    log = logging.getLogger(__name__)

    # who is currently holding the transaction-lock?
    # the stop event of the "grabbing transaction lock" span is the start of
    # holding the transaciton-lock and the info message "releasing the
    # transaction-lock" marks the end of it
    tlock_holder = None

    # this contains depth and spans keyed per tid
    tids = {}

    for pev in stream:
        kvs = pev.copy()
        for untag in ('type', 'timestamp'):
            if untag in kvs:
                del kvs[untag]
        to_delete = []
        for key, val in kvs.items():
            if val == '':
                to_delete.append(key)
        for key in to_delete:
            del kvs[key]

        if pev['tid'] not in tids:
            tids[pev['tid']] = {
                'depth': 0,
                'last_by_depth': {},
                'spans': []
            }
        depth = tids[pev['tid']]['depth']
        spans = tids[pev['tid']]['spans']

        # figure out our parent
        if depth == 0:
            parent_span = None
        else:
            parent_span = tids[pev['tid']]['last_by_depth'][depth-1]

        if pev['type'] == 1: # this is a start event, i.e. start of a span
            #print(f"{pev['tid']:10s}{depth}>>{'  '*depth} {pev['msg']}")
            span = tracer.start_span(pev['msg'],
                                     parent=parent_span,
                                     attributes=kvs,
                                     start_time=pev['timestamp']*1000)
            tids[pev['tid']]['last_by_depth'][depth] = span
            # for the start of the "grabbing transaction lock" span, if
            # we know the transaction-lock holder, add it as an event
            if pev['msg'] == 'grabbing transaction lock' and tlock_holder is not None:
                span.add_event('trans-lock holder changed', { 'transaction-lock-holder': tlock_holder }, pev['timestamp'] * 1000)

            spans.append(span)
            depth += 1

        elif pev['type'] == 2: # this is a stop event, i.e. end of a span
            #print(f"{pev['tid']:10s}{depth}<<{'  '*depth} {pev['msg']}")

            # maintain transaction-lock holder state
            # the stop message "releasing transaction lock" marks the
            # start of holding the transaction-lock
            if pev['msg'] == 'grabbing transaction lock':
                tlock_holder = pev['tid']
                tlock_span = tracer.start_span('holding transaction lock',
                                               parent=parent_span,
                                               attributes=kvs,
                                               start_time=pev['timestamp'] * 1000)
                tids[pev['tid']]['tlock_span'] = tlock_span
                # we just got the transaction-lock - find other
                # transactions waiting for the lock and add a KV-log
                # entry so it shows they are waiting for us
                # NOTE: this is a relatively expensive search, looping
                # through all current state
                for otid, oval in tids.items():
                    if otid == pev['tid']:
                        continue
                    for ospan in oval['spans']:
                        if ospan.end_time is None and ospan.name == 'grabbing transaction lock':
                            ospan.add_event('trans-lock-holder changed',
                                            {'transaction-lock-holder': tlock_holder},
                                            pev['timestamp'] * 1000)

            depth -= 1
            if depth < 0:
                log.warning("Stack depth should never be < 0. Likely startup during ongoing transaction - resetting stack.")
                depth = 0
                spans = {}

            # find matching start entry, it is the last entry with the same depth as us
            match = tids[pev['tid']]['last_by_depth'][depth]
            match.end(pev['timestamp']*1000)


        elif pev['type'] == 3:
            # maintain transaction-lock holder state
            # the info message "releasing transaction lock" marks the end
            if pev['msg'] == 'releasing transaction lock':
                tlock_holder = None
                if 'tlock_span' not in tids[pev['tid']]:
                    print(f"WARNING: trying to close trans-lock span, but none open - tid: {pev['tid']}")
                else:
                    tids[pev['tid']]['tlock_span'].end(pev['timestamp'] * 1000)

            for span in reversed(spans):
                if span.end_time is None:
                    span.add_event(pev['msg'], kvs, pev['timestamp'] * 1000)
                    break
        else:
            raise ValueError(f"Unhandled progress-trace event type {pev['type']}")

        # if we are at the end of the root span, clean up state
        if depth == 0:
            #print(f"{pev['tid']} == cleaning up")
            del tids[pev['tid']]
        else: # otherwise preserve our state
            tids[pev['tid']]['depth'] = depth
            tids[pev['tid']]['spans'] = spans

        # something is wrong if this ballons in size, 1000 is a very high limit
        # given how NSO normally operates, so this *must* be a bug somewhere.
        # Most likely unbalanced start / stop messages.
        assert len(tids) < 1000



def csv_ptrace_reader(filename):
    """Generator that yields progress-trace events as read from a
    progress-trace CSV file

    This will try to workaround deficiencies in older CSV exports where the
    type field isn't present. We try to recreate it based on the messages but
    this is somewhat error prone, at least theoretically, so don't be surprised
    if this breaks.
    """
    # This is a translating of CSV field names to the expected keys of a
    # progress-trace event as emitted by the NSO notification API
    xlate = {
        'TIMESTAMP': 'timestamp',
        'DURATION': 'duration',
        'SESSION ID': 'usid',
        'TID': 'tid',
        'TRANSACTION ID': 'tid',
        'DATASTORE': 'datastore_name',
        'CONTEXT': 'context',
        'SUBSYSTEM': 'subsystem',
        'PHASE': 'phase',
        'SERVICE': 'service',
        'SERVICE PHASE': 'service_phase',
        'COMMIT QUEUE ID': 'cqid',
        'NODE': 'node',
        'DEVICE': 'device',
        'DEVICE PHASE': 'device_phase',
        'PACKAGE': 'package',
        'MESSAGE': 'msg',
        'ANNOTATION': 'annotation'
    }

    # These are the start (1) and stop (2) messages for spans that have
    # unbalanced start and stop messages, i.e. where they are not the same.
    span_unbalanced = {
        'entering abort phase': 1,
        'entering commit phase': 1,
        'entering prepare phase': 1,
        'entering validate phase': 1,
        'entering write-start phase': 1,
        'leaving abort phase': 2,
        'leaving commit phase': 2,
        'leaving prepare phase': 2,
        'leaving validate phase': 2,
        'leaving write-start phase': 2
    }

    # Names of spans that have balanced start and stop messages
    span_names = {
        'applying FASTMAP reverse diff-set',
        'applying transaction',
        'check configuration policies',
        'check data kickers',
        'create',
        'creating rollback file',
        'grabbing transaction lock',
        'mark inactive',
        'post-modification',
        'pre validate',
        'pre-modification',
        'run dependency-triggered validation',
        'run pre-transform validation',
        'run service',
        'run transforms and transaction hooks',
        'run validation over the changeset',
        'saving FASTMAP reverse diff-set and applying changes',
    }

    # Known info messages. Anything not in here and not matching a list of
    # regexps will result in a warning - possibly something we want to look
    # into.
    known_info = {
        'all commit subscription notifications acknowledged',
        'commit',
        'conflict deleting zombie, adding re-deploy to sequential side effect queue',
        'nano service deleted',
        'prepare',
        're-deploy merged in queue',
        're-deploy queued',
        'received commit from all (available) slaves',
        'received prepare from all (available) slaves',
        'releasing device lock',
        'releasing transaction lock',
        'send NED commit',
        'send NED connect',
        'send NED get-trans-id',
        'send NED initialize',
        'send NED is-alive',
        'send NED persist',
        'send NED prepare',
        'send NED prepare-dry',
        'send NED reconnect',
        'send NED show',
        'sending confirmed commit',
        'sending confirming commit',
        'sending edit-config',
        'transaction empty',
        'write-start',
        'zombie deleted',
    }

    with open(filename) as csvfile:
        start_seen = {}

        csvreader = csv.DictReader(csvfile)
        for raw in csvreader:
            row = {}
            for key, val in raw.items():
                if key == 'TIMESTAMP':
                    # convert timestamp
                    ts = datetime.datetime.strptime(val, '%Y-%m-%dT%H:%M:%S.%f')
                    val = int(ts.strftime("%s%f"))
                row[xlate[key]] = val

            durm = re.search(r'(.*) \[([0-9]+) ms\]$', row['msg'])
            if durm:
                if row['duration'] != '':
                    print("ERROR: got duration in msg but duration set")
                row['msg'] = durm.group(1)
                row['duration'] = durm.group(2)

            if 'type' not in row:
                if row['msg'].endswith('...'):
                    row['type'] = 1
                    row['msg'] = row['msg'].replace('...', '')
                    start_seen[row['msg']] = True

                elif re.search(' (done|error|ok)$', row['msg']):
                    row['type'] = 2
                    row['msg'] = re.sub(' (done|error|ok)$', '', row['msg'])
                    if row['msg'] in start_seen:
                        del start_seen[row['msg']]

                elif row['msg'] in span_unbalanced:
                    row['type'] = span_unbalanced[row['msg']]

                elif row['msg'] in span_names:
                    if row['msg'] in start_seen:
                        row['type'] = 2
                        del start_seen[row['msg']]
                    else:
                        row['type'] = 1
                        start_seen[row['msg']] = True
                else:
                    row['type'] = 3
                    if re.match('delivering commit subscription notifications at prio', row['msg']):
                        pass
                    elif re.match('transaction lock queue length:', row['msg']):
                        pass
                    elif re.match('SNMP connect to', row['msg']):
                        pass
                    elif re.match('(SNMP|SSH) connecting to', row['msg']):
                        pass
                    elif re.match('reuse SSH connection', row['msg']):
                        pass
                    elif re.match('SNMP USM engine id', row['msg']):
                        pass
                    elif (re.match('evaluated behaviour tree', row['msg'])
                          or re.match('component.*state', row['msg'])
                         ):
                        pass
                    elif row['msg'] not in known_info:
                        print(f"WARNING: unknown message, assuming info: {row['msg']}")

            yield row


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--jaeger-host', default='localhost')
    parser.add_argument('--jaeger-port', type=int, default=6831)
    parser.add_argument('--csv')
    args = parser.parse_args()

    if args.csv:
        jaeger_exporter = jaeger.JaegerSpanExporter(
            service_name="NSO",
            agent_host_name=args.jaeger_host,
            agent_port=args.jaeger_port,
        )

        trace.set_tracer_provider(TracerProvider())
        trace.get_tracer_provider().add_span_processor(
            SimpleExportSpanProcessor(jaeger_exporter)
        )

        tracer = trace.get_tracer('opentelemetry-exporter')

        process_traces(tracer, csv_ptrace_reader(args.csv))
